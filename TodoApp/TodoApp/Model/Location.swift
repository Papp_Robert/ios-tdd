//
//  Location.swift
//  TodoApp
//
//  Created by Róbert PAPP on 2019. 04. 18..
//  Copyright © 2019. Róbert PAPP. All rights reserved.
//

import Foundation
import CoreLocation

struct Location : Equatable {
    let name: String
    let coordinate: CLLocationCoordinate2D?
    
    init(name: String,
         coordinate: CLLocationCoordinate2D? = nil) {
        self.name = name
        self.coordinate = coordinate
    }
}

func ==(lhs: Location, rhs: Location) -> Bool {
    if lhs.name != rhs.name {
        return false
    }
    if lhs.coordinate?.latitude != rhs.coordinate?.longitude {
        return false
    }
    if lhs.coordinate?.longitude != rhs.coordinate?.longitude {
        return false
    }
    return true
}

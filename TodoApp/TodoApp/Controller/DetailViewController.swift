//
//  DetailViewController.swift
//  TodoApp
//
//  Created by Róbert PAPP on 2019. 05. 01..
//  Copyright © 2019. Róbert PAPP. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {
    
    var itemInfo: (ItemManager, Int)?
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var mapView: MKMapView!
    
    let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        return dateFormatter
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let itemInfo = itemInfo else { return }
        let item = itemInfo.0.item(at: itemInfo.1)
        
        titleLabel.text = item.title
        
        if let coordinate = item.location?.coordinate{
            let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 100,longitudinalMeters: 100)
            mapView.region = region
        }
    }
    
    func checkItem(){
        if let itemInfo = itemInfo {
            itemInfo.0.checkItem(at: itemInfo.1)
        }
    }
    
}

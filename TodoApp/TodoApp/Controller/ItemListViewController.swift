//
//  ItemListViewController.swift
//  TodoApp
//
//  Created by Róbert PAPP on 2019. 04. 23..
//  Copyright © 2019. Róbert PAPP. All rights reserved.
//

import UIKit

class ItemListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var dataProvider: (UITableViewDataSource & UITableViewDelegate)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = dataProvider
        tableView.delegate = dataProvider
    }
    
}

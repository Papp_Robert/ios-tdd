//
//  InputViewController.swift
//  TodoApp
//
//  Created by Róbert PAPP on 2019. 05. 01..
//  Copyright © 2019. Róbert PAPP. All rights reserved.
//

import UIKit
import CoreLocation

class InputViewController: UIViewController {

    @IBOutlet weak var titleTextField: UILabel!
    
    @IBOutlet weak var dateText: UILabel!
    
    @IBOutlet weak var locationTextField: UILabel!
    
    @IBOutlet weak var addressTextField: UILabel!
    
    @IBOutlet weak var saveButton: UIButton!
    
    lazy var geocoder = CLGeocoder()

    var itemManager: ItemManager?
    
    let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        return dateFormatter
    }()
    
    @IBAction func save(_ sender: Any) {
        guard let titleString = titleTextField.text, titleString.count > 0 else {
            return
        }
        let date: Date?
        if let dateTextString = self.dateText.text,
            dateTextString.count > 0 {
            date = dateFormatter.date(from: dateTextString)
        } else {
            date = nil
        }
        
        if let locationName = locationTextField.text,
            locationName.count > 0 {
            if let address = addressTextField.text,
                address.count > 0 {
                
                geocoder.geocodeAddressString(address) {
                    [unowned self] (placeMarks, error) -> Void in
                    
                    let placeMark = placeMarks?.first
                    let item = ToDoItem(title: titleString, itemDescription: nil, timestamp: date?.timeIntervalSince1970, location: Location(name: locationName, coordinate: placeMark?.location?.coordinate))
                    
                    self.itemManager?.add(item)
                }
                
            }
        }
    }
}

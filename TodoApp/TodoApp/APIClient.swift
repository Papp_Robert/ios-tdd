//
//  APIClient.swift
//  TodoApp
//
//  Created by Róbert PAPP on 2019. 05. 02..
//  Copyright © 2019. Róbert PAPP. All rights reserved.
//

import Foundation

class APIClient {
    lazy var session: SessionProtocol = URLSession.shared
    
    func loginUser(withName username: String,
                   password: String,
                   completion: @escaping (Token?, Error?) -> Void) {
        
    }
    
}

protocol SessionProtocol {
    func dataTask(
        with url: URL,
        completionHandler: @escaping
        (Data?, URLResponse?, Error?) -> Void)
    -> URLSessionDataTask
}

extension URLSession: SessionProtocol { }

//
//  ApiClientTests.swift
//  TodoAppTests
//
//  Created by Róbert PAPP on 2019. 05. 02..
//  Copyright © 2019. Róbert PAPP. All rights reserved.
//

import XCTest
@testable import TodoApp

class ApiClientTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    
    func test_Login_UsesExceptedHost() {
        let sut = APIClient()
        let mockURLSession = MockURLSession()
        
        sut.session = mockURLSession
        
        let completion = { (token: Token?, error: Error?) in}
        sut.loginUser(withName: "dasdom",
                      password: "1234",
                      completion: completion)
        
        guard let url = mockURLSession.url else { XCTFail(); return }
        let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
        
        XCTAssertEqual(urlComponents?.host, "awesometodos.com")
    }

}

extension ApiClientTests {
    
    class MockURLSession: SessionProtocol {
        var url: URL?
        
        func dataTask(
            with url: URL,
            completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
                self.url = url
                return URLSession.shared.dataTask(with: url)
            }
        }
}

//
//  FirstDemoTests.swift
//  FirstDemoTests
//
//  Created by Róbert PAPP on 2019. 04. 17..
//  Copyright © 2019. Róbert PAPP. All rights reserved.
//

import XCTest
@testable import FirstDemo

class FirstDemoTests: XCTestCase {

    var viewController: ViewController!
    
    override func setUp() {
        super.setUp()
        viewController = ViewController()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_NumberOfVowels_WhenPassedDominik_ReturnsThree() {
        let string = "Dominik"
        
        let numberOfVowels = viewController.numberOfVowels(in: string)
        
        XCTAssertEqual(numberOfVowels, 3)
    }
    
    func test_MakeHeadline_ReturnsStringWithEachWordStartCapital(){
        let string = "this is A test headline"
        let expectedOutput = "This Is A Test Headline"
        
        let headline = viewController.makeHeadline(from: string)
        
        XCTAssertEqual(headline, expectedOutput)
    }
    
    func test_MakeHeadline_ReturnsStringWithEachWordStartCapital2(){
        
        let string = "Here is another Example"
        let expectedOutput = "Here Is Another Example"
        
        let headline = viewController.makeHeadline(from: string)
        
        XCTAssertEqual(headline, expectedOutput)
    }

}
